package com.fyntem.lab6springcontroller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lab6SpringControllerApplication {

    public static void main(String[] args) {
        SpringApplication.run(Lab6SpringControllerApplication.class, args);
    }

}
